import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { FileReaderService } from '@app/services/file-reader.service';
@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-wish-list',
  styleUrls: ['./wish-list.page.less'],
  templateUrl: './wish-list.page.html'
})
export class WishListPage implements OnDestroy {

  constructor(private _fileReaderService: FileReaderService){}

  public ngOnDestroy(): void {
    this._fileReaderService.fileData$.next(null);
  }
}
