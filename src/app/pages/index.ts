import { GamesListPage } from './games-list/games-list.page';
import { WishListPage } from './wish-list/wish-list.page';

export const pages = [GamesListPage, WishListPage];
