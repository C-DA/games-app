import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy } from '@angular/core';
import { FileReaderService } from '@app/services/file-reader.service';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-games-list',
  styleUrls: ['./games-list.page.less'],
  templateUrl: './games-list.page.html'
})
export class GamesListPage implements OnDestroy {
  public data: Array<Array<any>>
  public firstCardFullScreen: boolean = true;
  public dataTypeName: string;
  public secondCardFullScreen: boolean = false;

  constructor(private _changeDetectorRef: ChangeDetectorRef, private _fileReaderService: FileReaderService) {}

  public onFirstCardChange(toggle: boolean): void {
    this.firstCardFullScreen = toggle;
    if (toggle) {
      this.secondCardFullScreen = false;
    }
    this._changeDetectorRef.detectChanges();
  }

  public onSecondCardChange(toggle: boolean): void {
    this.secondCardFullScreen = toggle;
    if(toggle) {
      this.firstCardFullScreen = false;
    }
    this._changeDetectorRef.detectChanges();
  }

  public getFilesData(filesData: Array<Array<any>>): void {    
    if (!filesData || !filesData.length) return;
   this.data = [...filesData];
   this._changeDetectorRef.detectChanges();
  }

  public getFileTabName(tabName: string): void {
    this.dataTypeName = tabName;
  }

  public ngOnDestroy(): void {
    this._fileReaderService.fileData$.next(null);
  }
}
