import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GamesListPage } from './pages/games-list/games-list.page';
import { WishListPage } from './pages/wish-list/wish-list.page';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'games-list' },
  { path: 'games-list', component: GamesListPage },
  { path: 'wish-list', component: WishListPage },
  { path: '**', pathMatch: 'full', redirectTo: 'games-list' }
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes)]
})
export class AppRoutingModule {}
