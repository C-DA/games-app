import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ENavigation } from '../../../enums/navigation.enum';

@Component({
  selector: 'app-header',
  styleUrls: ['./header.component.less'],
  templateUrl: './header.component.html',
})
export class HeaderComponent {
  public ENavigation = ENavigation;

  constructor(private _router: Router) { }

  public isNavItemActive(route: ENavigation): boolean {
    return this._router.isActive(route, false);
  }

  public goTo(route: ENavigation): void {
    this._router.navigate([route]);
  }
}
