import { animate, state, style, transition, trigger } from '@angular/animations';
import { AfterViewInit, ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, Output, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Chart } from 'chart.js';
import { FileReaderService } from '@app/services/file-reader.service';
import { DataFileModel } from '@app/models/data-files.model';

@Component({
  animations: [       // metadata array
    trigger('toggleClick', [     // trigger block
      transition('true => false', animate('1000ms linear')),  // animation timing
      transition('false => true', animate('1000ms linear')),
      state('true', style({      // final CSS following animation
        opacity: 1
      })),
      state('false', style({
        background: '#80808040',
        height: '65px',
        opacity: 0.25,
        overflow: 'hidden'
      }))
    ])
  ],
changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-video-games-pie-chart',
  styleUrls: ['./video-games-pie-chart.component.less'],
  templateUrl: './video-games-pie-chart.component.html'
})
export class VideoGamesPieChartComponent implements AfterViewInit {
  @Input() public set filesData (data: Array<Array<any>>){
    if(!data || !data.length) return;
    this._calculateTabSelectedDataRepartition(data);
    this._filesData = data;
    this.getAllFilesData();
    this.allDataNoTested = this._getAllDataWithStatus('NoTested');
    this._setConfig();
  }
  @Input() public dataTypeName: string;
  @Input() public fullScreen: boolean = false;
  @ViewChild('chartjs', { static: true }) chartjs$: ElementRef<HTMLCanvasElement>;
  @Output() public fullScreenEvent = new EventEmitter<boolean>();

  public allDataUse = false;
  public allDataNoTested: Array<Array<any>> = [];
  public canvas: any;
  public ctx: any;
  public pieChartInitiate: boolean;
  public videoGamesRepartition: Array<number>; // first data NoTested | Tested | Finished
  public allFileDatas: DataFileModel;

  private _chartInstance: Chart;
  private _filesData: Array<Array<any>>;

  constructor(private _changeDetectorRef: ChangeDetectorRef, private _fileReaderService: FileReaderService) {}

  public toogleFullScreen(): void {
    this.fullScreen = !this.fullScreen;
    this.fullScreenEvent.emit(this.fullScreen);
  }

  public ngAfterViewInit(): void {
    this.canvas = document.getElementById('myChart');
    this.ctx = this.canvas.getContext('2d');
    this._setConfig();
  }

  public switchData(): void {
    this.allDataUse = !this.allDataUse;
    if (this.allDataUse) {
      this._calculateAllFilesDataRepartition();
    } else {
      this._calculateTabSelectedDataRepartition(this._filesData);
    }
    this._setConfig();
  }

  public getAllFilesData(): void {
    this.allFileDatas = this._fileReaderService.fileData;
  }

  public nextVideoGames(): void {
    const noTestedGames = this.allDataUse ? this.allDataNoTested : this._filesData.filter((data) => data[3] === 'NoTested');
    const countTotalNoTested = this.allDataUse ? this._countAllFiles('NoTested') : this._countFilesDataWithStatus(this._filesData,'NoTested');
    const index = Math.floor(Math.random()*countTotalNoTested);
    alert(noTestedGames[index][1]);
  }

  private _calculateTabSelectedDataRepartition(filesData: Array<Array<any>>): void {
    this.videoGamesRepartition = [
      this._countFilesDataWithStatus(filesData, 'NoTested'),
      this._countFilesDataWithStatus(filesData, 'Tested'),
      this._countFilesDataWithStatus(filesData, 'Finished')
    ];
  }

  private _calculateAllFilesDataRepartition(): void {
    this.videoGamesRepartition = [
      this._countAllFiles('NoTested'),
      this._countAllFiles('Tested'),
      this._countAllFiles('Finished')
    ];
  }

    private _countAllFiles(status: 'NoTested' | 'Tested' | 'Finished'): number {
      let totalCountNumber = 0;
      this.allFileDatas.data.forEach((dataModel) => {
        totalCountNumber += this._countFilesDataWithStatus(dataModel.content, status);
      });
      return totalCountNumber;
    }

  private _countFilesDataWithStatus(filesData: Array<Array<any>>, status: 'NoTested' | 'Tested' | 'Finished'): number {
    let count = 0;
    for (const data of filesData) {
      for (const property of data) {
        if (property === status) {
          count++;
          continue;
        }
      }
    }
    return count;
  }


  private _getAllDataWithStatus(status: 'NoTested' | 'Tested' | 'Finished'): Array<Array<any>> {
    const tab = [];
    this.allFileDatas.data.forEach((dataModel) => {
      for (const data of dataModel.content) {
        for (const property of data) {
          if (property === status) {
            if(status === 'NoTested'){
              tab.push(data);
            }
            continue;
          }
        }
      }
    });
    return tab;
  }

  private _setConfig(): void {
    if (!this.chartjs$) return;
    this._changeDetectorRef.detectChanges();


    if (!this._chartInstance) {
      this._chartInstance = new Chart(this.ctx, {
        data: {
          datasets: [
            {
              backgroundColor: ['rgba(255, 99, 132, 1)', 'rgba(54, 162, 235, 1)', 'rgba(255, 206, 86, 1)'],
              borderWidth: 1,
              data: this.videoGamesRepartition,
              label: '# of Votes'
            }
          ],
          labels: ['NoTested', 'Tested', 'Finished']
        },
        options: {
          legend: {
            position: 'bottom'
          },
          responsive: false
        },
        type: 'pie'
      });
    }

    this._chartInstance.data.datasets = [
      {
        backgroundColor: ['rgba(255, 99, 132, 1)', 'rgba(54, 162, 235, 1)', 'rgba(255, 206, 86, 1)'],
        borderWidth: 1,
        data: this.videoGamesRepartition,
        label: '# of Votes'
      }
    ];
    this._chartInstance.update();
  }

}
